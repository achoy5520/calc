#include "Expression.h"
#include <queue>

vector<string> letterCombinationsUtil(const int number[], int n, const string table[]) {
    // To store the generated letter combinations
    vector<string> list;
    vector<int> tmp;

    queue<string> q;
    q.push("");


    while (!q.empty()) {
        string s = q.front();
        q.pop();

        // If complete word is generated push it in the list
        if ((int) s.length() == n) {
            list.push_back(s);
            for (auto num : tmp)
                cout << num << " ";
            cout << endl;

        } else {
            // Try all possible letters for current digit in number[]
//            cout << s.length() << ", " << number[s.length()] << ", " << table[number[s.length()]] << endl;
            tmp.insert(tmp.begin() + s.length(), number[s.length()]);

            for (auto letter : table[number[s.length()]]) {
                q.push(s + letter);
//                cout << s.length() << ", " << number[s.length()] << ", " << table[number[s.length()]] << endl;
            }
        }
    }



    // Return the generated list
    return list;
}

// Function that creates the mapping and
// calls letterCombinationsUtil
void letterCombinations(const int number[], int n) {
    // table[i] stores all characters that
    // corresponds to ith digit in phone
    string table[10] = {"oqz", "ij", "abc", "def", "gh",
                        "kl", "mn", "prs", "tuv", "wxy"};

    vector<string> list = letterCombinationsUtil(number, n, table);

    // Print the contents of the vector
    for (auto word : list)
        cout << word << " ";

    return;
}

string convertWord2Digit(string s) {
    string conv = "";

    for (int i = 0; i < (int) s.size(); i++) {
        if (s[i] == 'i' || s[i] == 'j') conv += "1";
        else if (s[i] == 'a' || s[i] == 'b' || s[i] == 'c') conv += "2";
        else if (s[i] == 'd' || s[i] == 'e' || s[i] == 'f') conv += "3";
        else if (s[i] == 'g' || s[i] == 'h') conv += "4";
        else if (s[i] == 'k' || s[i] == 'l') conv += "5";
        else if (s[i] == 'm' || s[i] == 'n') conv += "6";
        else if (s[i] == 'p' || s[i] == 'r' || s[i] == 's') conv += "7";
        else if (s[i] == 't' || s[i] == 'u' || s[i] == 'v') conv += "8";
        else if (s[i] == 'w' || s[i] == 'x' || s[i] == 'y') conv += "9";
        else conv += "0";
    }

    return conv;
}

int main() {
//    int phoneNumber[] = {7, 3, 2, 5, 1, 8, 9, 0, 8, 7};
//    int num = sizeof(phoneNumber) / sizeof(phoneNumber[0]);
//    letterCombinations(phoneNumber, num);

    string phoneNumber, word;
    vector<string> dictionary;
    vector<string> converted;
    int num;
    int dp[101];

    while (cin >> phoneNumber) {
        if (phoneNumber == "-1") break;

        cin >> num; //

        dictionary.clear();
        converted.clear();

        for (int i = 0; i < num; i++) {
            cin >> word;
            dictionary.push_back(word);
            converted.push_back(convertWord2Digit(word));
        }

        fill(dp, dp + phoneNumber.size() + 1, (1 << 20)); //limit 100 digits
        dp[0] = 0;
        vector<string> sequence(phoneNumber.size() + 1, "");

        for (int i = 1; i <= (int) phoneNumber.size(); i++) {
            for (int j = 0; j < num; j++) {
                if ((int) converted[j].size() <= i &&
                    phoneNumber.substr(i - converted[j].size(), converted[j].size()) == converted[j] &&
                    dp[i] > 1 + dp[i - converted[j].size()]) {
//                    cout << "`i: " << i << ", j: " << j << ", inx: " << (i - converted[j].size())
//                         << ", seq: " << sequence[i - converted[j].size()]
//                         << ", conv: " << converted[j]
//                         << ", num: " << phoneNumber.substr(i - converted[j].size(), converted[j].size())
//                         << ", str: " << sequence[i - converted[j].size()] + dictionary[j]
//                         << endl;

                    dp[i] = 1 + dp[i - converted[j].size()];
                    sequence[i] = sequence[i - converted[j].size()] + dictionary[j] + " ";
                }
            }
        }

        if (dp[phoneNumber.size()] == (1 << 20)) cout << "No solution." << endl;
        else {
            word = sequence[phoneNumber.size()];
            word.erase(word.size() - 1, 1);
            cout << word << endl;
        }
    }

//    Expression expr;
//    string expression;
//
//    while (getline(cin, expression)) {
//        if (expression == "q") break;
//        float result = expr.calc(expression);
//        cout << "Result: " << result << endl;
//    }

    return 0;
}
